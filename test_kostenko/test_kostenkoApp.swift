//
//  test_kostenkoApp.swift
//  test_kostenko
//
//  Created by Aliona Kostenko on 28.02.2023.
//

import SwiftUI

@main
struct test_kostenkoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

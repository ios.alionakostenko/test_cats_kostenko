//
//  ContentView.swift
//  test_kostenko
//
//  Created by Aliona Kostenko on 28.02.2023.
//

import SwiftUI
import Foundation
import Combine

struct Item: Codable {
    let title: String?
    let firstImg: String?
    let secondImg: String?
    let thirdImg: String?
    let details: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case firstImg = "firstimg"
        case secondImg = "secondimg"
        case thirdImg = "thirdimg"
        case details
    }
}

struct ContentView: View {
    @State private var itemResult = [Item]()
    @State private var allImages = [String: UIImage]()
    @State var isAllImagesLoaded = false
    var body: some View {
        NavigationView {
            List(itemResult, id: \.title) { item in
                if isAllImagesLoaded {
                    NavigationLink(destination: DetailView(selectedItem: item, image: findImageForSelectedItem(item))) {
                        VStack(alignment: .leading) {
                            HStack(alignment: .center, spacing: 20) {
                                Text(String(item.title ?? ""))
                                Text(String(item.details ?? ""))
                            }
                        }
                    }
                } else {
                    Text("Loading...")
                        .onAppear {
                            loadImagesFromItems(itemResult)
                        }
                }
            }
        }
        .onAppear() {
            loadData()
        }
    }
    
    func loadData() {
        let arrayOfStrings = fetchDataFromJson()
        var arrayOfDicts: [[String: String]] = []
        for str in arrayOfStrings {
            var dict: [String: String] = [:]
            let components = createComponent(str)
            
            for component in components {
                var refreshedComponent = component
                if component.hasPrefix(",") {
                    refreshedComponent = component.replacingOccurrences(of: ",", with: "")
                }
                let keyValue = refreshedComponent.split(separator: ":", maxSplits: 1)
                let key = keyValue[0].trimmingCharacters(in: .whitespaces)
                let value = keyValue[1].trimmingCharacters(in: .whitespaces)
                dict[key] = value
            }
            arrayOfDicts.append(dict)
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: arrayOfDicts, options: .prettyPrinted)
        guard let item = try? JSONDecoder().decode([Item].self, from: jsonData) else { return }
        self.itemResult = item
    }
    
    func fetchDataFromJson() -> [String] {
        if let path = Bundle.main.path(forResource: "task", ofType: "json") {
            do {
                let jsonData = try String(contentsOfFile: path).data(using: .utf8)
                let arrayOfStrings = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String]
                return arrayOfStrings
            } catch {
                print(error.localizedDescription)
            }
        }
        return []
    }
    
    func createComponent(_ srtingValue: String) -> [String] {
        var components: [String] = []
        let regex = try! NSRegularExpression(pattern: ",(?!\\s|\\d)")
        let range = NSRange(srtingValue.startIndex..<srtingValue.endIndex, in: srtingValue)
        let results = regex.matches(in: srtingValue, range: range)
        
        var previousIndex = srtingValue.startIndex
        for result in results {
            let startIndex = srtingValue.index(srtingValue.startIndex, offsetBy: result.range.lowerBound)
            let substring = srtingValue[previousIndex..<startIndex].trimmingCharacters(in: .whitespacesAndNewlines)
            components.append(substring)
            previousIndex = startIndex
        }
        
        let lastSubstring = srtingValue[previousIndex...].trimmingCharacters(in: .whitespacesAndNewlines)
        components.append(lastSubstring)
        return components
    }
    
    func loadImagesFromItems(_ itemResult: [Item]) {
        let url1 = itemResult.map { $0.firstImg }
        let url2 = itemResult.map { $0.secondImg }
        let url3 = itemResult.map { $0.thirdImg }
        let urls = url1 + url2 + url3
        var dict: [String: UIImage] = [:]
        
        let dispatchGroup = DispatchGroup()
        
        for url in urls {
            dispatchGroup.enter()
            guard let urlKey = url else { return }
            if let url = URL(string: urlKey) {
                let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    defer { dispatchGroup.leave() }
                    if error != nil {
                        print("Error loading image: \(error!.localizedDescription)")
                    } else if let imageData = data, let image = UIImage(data: imageData) {
                        dict[urlKey] = image
                        self.allImages = dict
                    }
                })
                task.resume()
            }
            dispatchGroup.notify(queue: DispatchQueue.main) {
                self.isAllImagesLoaded = true
            }
        }
    }

    func findImageForSelectedItem(_ selectedItem: Item) -> [UIImage]? {
        let imageNames = [selectedItem.firstImg, selectedItem.thirdImg, selectedItem.secondImg]
        let images = imageNames.compactMap { allImages[$0 ?? ""] }
        return images
    }
}

struct DetailView: View {
    var selectedItem: Item?
    var image: [UIImage]?
    
    var body: some View {
        VStack {
            Text("\(selectedItem?.title ?? "")")
            if let image = image {
                Image(uiImage: image[0])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Image(uiImage: image[1])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Image(uiImage: image[2])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
        }
    }
}
